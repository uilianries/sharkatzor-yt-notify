import requests
import googleapiclient.discovery
from googleapiclient.errors import HttpError
import logging
import argparse
import configparser


LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER.setFormatter(formatter)
LOGGER.addHandler(S_HANDLER)


class YoutubeVideo(object):
    def __init__(self, id="", title=""):
        self.id = id
        self.title = title
        self._config = configparser.ConfigParser()
        self._config["video"] = {"id": self.id, "title": self.title}

    def __eq__(self, other):
        return self.id == other.id

    def store(self, json_path):
        with open(json_path, 'w') as fd:
            self._config.write(fd)

    @property
    def link(self):
        return f"https://www.youtube.com/watch?v={self.id}"

    @staticmethod
    def generate(snippet):
        return YoutubeVideo(snippet["resourceId"]["videoId"], snippet["title"])

    @staticmethod
    def restore(json_path):
        try:
            config = configparser.ConfigParser()
            config.read(json_path)
            return YoutubeVideo(config["video"]["id"], config["video"]["title"])
        except Exception:
            return YoutubeVideo()


class YoutubeScrapper:

    def __init__(self, logger, gcp_api_keys):
        self._logger = logger
        self._youtube = None
        self._gcp_api_keys = gcp_api_keys
        self._playlist = None

    def _login_youtube(self, channel_id):
        self._logger.debug("Executing YT login")
        for key in self._gcp_api_keys:
            try:
                self._logger.info("Connecting to YT with key {}****".format(key[:8]))
                self._youtube = googleapiclient.discovery.build("youtube", "v3", developerKey=key)
                request = self._youtube.channels().list(part="id,contentDetails", id=channel_id, maxResults=1)
                response = request.execute()
                if not response:
                    message = "Could not login on Youtube!"
                    self._logger.error(message)
                self._playlist = response["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"]
                self._logger.info(f"Logged-in on youtube, Playlist ID: {self._playlist}")
                return
            except HttpError as err:
                self._logger.error(err.reason)
                pass
        raise Exception("Could not login on YT! Giving up!")

    def get_latest_video(self, channel_id):
        if self._youtube is None:
            self._login_youtube(channel_id)
        request = self._youtube.playlistItems().list(part="id,snippet", playlistId=self._playlist, maxResults=1)
        response = request.execute()
        if not response:
            raise Exception(f"Could not scrap YT channel {channel_id}!")
        snippet = response["items"][0]["snippet"]
        video = YoutubeVideo.generate(snippet)
        return video


class DiscordMessenger:

    def __init__(self, logger, access_token, public_channel, maintenance_channel):
        self._logger = logger
        self._public_channel = public_channel
        self._maintenance_channel = maintenance_channel
        self._access_token = access_token

    def _send_message(self, channel, message):
        response = requests.post(
            f"https://discordapp.com/api/channels/{channel}/messages",
            headers={"Authorization": f"Bot {self._access_token}"},
            json={"content": message}
        )
        response.raise_for_status()
        json_data = response.json()
        return json_data['id']

    def send_general_message(self, message):
        self._logger.debug("Sending message to general channel on Discord: {}".format(message))
        self._send_message(self._public_channel, message)

    def send_maintenance_message(self, message):
        self._logger.debug("Sending message to maintenance channel on Discord: {}".format(message))
        self._send_message(self._maintenance_channel, message)


class SharkatzorConfiguration:

    def __init__(self, logger, config_file_path):
        self._logger = logger
        self._config = self._read_config(config_file_path)

    def _read_config(self, config_file_path):
        config = configparser.ConfigParser()
        config.read(config_file_path)
        return config

    def _log_configuration(self):
        self._logger.info(f'Youtube channel ID: {self._config["youtube"]["channel_id"]}')
        self._logger.info('Discord Token: {}****'.format(self._config["discord"]["token"][:4]))
        self._logger.info('General Discord channel: {}****'.format(self._config["discord"]["general_channel"][:4]))
        self._logger.info('Maintenance Discord channel: {}****'.format(self._config["discord"]["maintenance_channel"][:4]))
        self._logger.info('Youtube keys (number): {}'.format(len(self._config["youtube"]["keys"].split(','))))

    @property
    def configuration(self):
        return self._config


class SharkatzorYoutubeNotifier:

    def __init__(self, config_path):
        self._logger = LOGGER
        self._logger.info('Starting ...')
        self._config = self._load_configuration(config_path)

    def _load_configuration(self, config_path):
        shark_config = SharkatzorConfiguration(self._logger, config_path)
        return shark_config.configuration

    def run(self):
        youtube = YoutubeScrapper(self._logger, self._config["youtube"]["keys"].split(','))
        latest_video = youtube.get_latest_video(self._config["youtube"]["channel_id"])
        self._logger.debug(f"Latest video on YT: {latest_video.id}")
        cache_video = YoutubeVideo.restore(self._config["store"]["path"])
        if cache_video != latest_video:
            self._logger.info(f"New YT video: ({latest_video.id}): '{latest_video.title}'")
            discord = DiscordMessenger(self._logger, self._config["discord"]["token"], self._config["discord"]["general_channel"], self._config["discord"]["maintenance_channel"])
            try:
                discord.send_general_message(f"Vídeo novo do Tomahawk no Youtube @everyone!\n**{latest_video.title}**\n{latest_video.link}")
                latest_video.store(self._config["store"]["path"])
            except Exception as err:
                self._logger.error(f"Could not send message to Discord: {err}")
                discord.send_maintenance_message(f"Caramba, não consegui mandar mensagem no Discord!:\n{err}")


def main():
    argparser = argparse.ArgumentParser(description="Sharkatzor Youtube Notifier for Discord")
    argparser.add_argument("-c", "--config", help="Configuration file path", default="/etc/sharkatzor/conf.ini")
    args = argparser.parse_args()

    sharkatzor = SharkatzorYoutubeNotifier(args.config)
    sharkatzor.run()


if __name__ == "__main__":
    main()

